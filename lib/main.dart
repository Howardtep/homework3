import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(5),
      child:Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('Please click the icon of what you would like to order!', style:
                  TextStyle(height: 1, fontSize: 20),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );

    Widget textSection = Container(
      padding: const EdgeInsets.all(24),
      child: Text(
        'Thank you for your order. You will receive your order confirmation in your email.'
        ' Please remember that all orders take time to process so we wish you wait in a timely matter.'
        ' If you have any questions about your order, please contact our email or call us. We wish you'
        ' have a great day and thank you for shopping with us!', style: TextStyle(height: 1.5, fontSize: 14),
      ),
    );

    return MaterialApp(
        title: 'Ordering App',
        theme: ThemeData(
          primarySwatch: Colors.red,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Scaffold(
            appBar: AppBar(
              title: Text('Order our supplies!'),
            ),
            body:ListView(
                children: [
                  Image.asset(
                    'images/Sale.jpg',
                    width: 500,
                    height: 180,
                    fit: BoxFit.cover,
                  ),
                  titleSection,
                  FavoriteWidget(),
                  UtilityWidget(),
                  EtcWidget(),
                  FoodWidget(),
                  textSection
                  //UtilityWidget(),
                ]
            )
        )
    );
  }
}

class FavoriteWidget extends StatefulWidget {
  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget> {
  bool _isFavorited = false;
  int _favoriteCount = 69;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(
          child: Container(
            padding: EdgeInsets.all(8),
              child: Text('Please like our App!', style: TextStyle(
                  height: 1, fontSize: 16),
              )),
        ),
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isFavorited ? Icon(Icons.favorite) :
            Icon(Icons.favorite_border)), color: Colors.red[500],
            onPressed: _toggleFavorite,
          ),
        ),
        SizedBox(
          width: 18,
          child: Container(
            child: Text('$_favoriteCount'),
          ),
        ),
      ],
    );
  }
  void _toggleFavorite() {
    setState(() {
      if(_isFavorited) {
        _favoriteCount -= 1;
        _isFavorited = false;
      } else {
        _favoriteCount += 1;
        _isFavorited = true;
      }
    });
  }
}

class FoodWidget extends StatefulWidget {
  @override
  _FoodWidgetState createState() => _FoodWidgetState();
}

class _FoodWidgetState extends State<FoodWidget> {
  bool _isFoodOrdered = false;
  bool _isPizzaOrdered = false;
  bool _isCakeOrdered = false;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isFoodOrdered ? Icon(Icons.fastfood, color: Colors.red[500]) :
            Icon(Icons.fastfood, color: Colors.grey[500])),
            onPressed: _toggleFoodOrdered,
          ),
        ),
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isPizzaOrdered ? Icon(Icons.local_pizza, color: Colors.red[500]) :
            Icon(Icons.local_pizza, color: Colors.grey[500])),
            onPressed: _togglePizzaOrdered,
          ),
        ),
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isCakeOrdered ? Icon(Icons.cake, color: Colors.red[500]) :
            Icon(Icons.cake, color: Colors.grey[500])),
            onPressed: _toggleAlarmOrdered,
          ),
        ),
      ],
    );
  }

  void _toggleFoodOrdered() {
    setState(() {
      if (_isFoodOrdered)
        _isFoodOrdered = false;
      else
        _isFoodOrdered = true;
    });
  }

  void _togglePizzaOrdered() {
    setState(() {
      if (_isPizzaOrdered)
        _isPizzaOrdered = false;
      else
        _isPizzaOrdered = true;
    });
  }

  void _toggleAlarmOrdered() {
    setState(() {
      if (_isCakeOrdered)
        _isCakeOrdered = false;
      else
        _isCakeOrdered = true;
    });
  }
}

class UtilityWidget extends StatefulWidget {
  @override
  _UtilityWidgetState createState() => _UtilityWidgetState();
}

class _UtilityWidgetState extends State<UtilityWidget> {
  bool _isBikeOrdered = false;
  bool _isCameraOrdered = false;
  bool _isTvOrdered = false;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isBikeOrdered ? Icon(Icons.directions_bike, color: Colors.red[500]) :
            Icon(Icons.directions_bike, color: Colors.grey[500])),
            onPressed: _toggleBikeOrdered,
          ),
        ),
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isCameraOrdered ? Icon(Icons.camera_alt, color: Colors.red[500]):
            Icon(Icons.camera_alt, color: Colors.grey[500])),
            onPressed: _toggleCameraOrdered,
          ),
        ),
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isTvOrdered ? Icon(Icons.tv, color: Colors.red[500]):
            Icon(Icons.tv, color: Colors.grey[500])),
            onPressed: _toggleTvOrdered,
          ),
        ),
      ],
    );
  }

  void _toggleBikeOrdered() {
    setState(() {
      if(_isBikeOrdered)
        _isBikeOrdered = false;
      else
        _isBikeOrdered = true;
    });
  }
  void _toggleCameraOrdered() {
    setState(() {
      if(_isCameraOrdered)
        _isCameraOrdered = false;
      else
        _isCameraOrdered = true;
    });
  }
  void _toggleTvOrdered() {
    setState(() {
      if(_isTvOrdered)
        _isTvOrdered = false;
      else
        _isTvOrdered = true;
    });
  }
}

class EtcWidget extends StatefulWidget {
  @override
  _EtcWidgetState createState() => _EtcWidgetState();
}

class _EtcWidgetState extends State<EtcWidget> {
  bool _isGameOrdered = false;
  bool _isToolOrdered = false;
  bool _isAlarmOrdered = false;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isGameOrdered ? Icon(Icons.videogame_asset, color: Colors.red[500]):
            Icon(Icons.videogame_asset, color: Colors.grey[500])),
            onPressed: _toggleGameOrdered,
          ),
        ),
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isToolOrdered ? Icon(Icons.build, color: Colors.red[500]):
            Icon(Icons.build, color: Colors.grey[500])),
            onPressed: _toggleToolOrdered,
          ),
        ),
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isAlarmOrdered ? Icon(Icons.alarm, color: Colors.red[500]):
            Icon(Icons.alarm, color: Colors.grey[500])),
            onPressed: _toggleAlarmOrdered,
          ),
        ),
      ],
    );
  }

  void _toggleGameOrdered() {
    setState(() {
      if(_isGameOrdered)
        _isGameOrdered = false;
      else
        _isGameOrdered = true;
    });
  }
  void _toggleToolOrdered() {
    setState(() {
      if(_isToolOrdered)
        _isToolOrdered = false;
      else
        _isToolOrdered = true;
    });
  }
  void _toggleAlarmOrdered() {
    setState(() {
      if(_isAlarmOrdered)
        _isAlarmOrdered = false;
      else
        _isAlarmOrdered = true;
    });
  }
}